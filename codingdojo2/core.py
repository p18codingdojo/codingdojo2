def bowling(line):
    frameArray = line.split(" ")
    score = 0
    for i in range(0,10):
        score += score_frame(frameArray[i], frameArray, i, 0)
    return score


def score_frame(currentFrame, allFrames, i, depth):
    if isStrike(currentFrame):
        return score_strike(allFrames, i, depth)
    elif isSpare(currentFrame):
        return score_spare(allFrames, i, depth)
    else:
        score = int(currentFrame[0])
        if len(currentFrame) > 1:
            score += int(currentFrame[1])
        return score


def score_spare(allFrames, i, depth):
    if depth >= 1:
        if isStrike(allFrames[i]) or isSpare(allFrames[i]):
            return 10
        else:
            return(int(allFrames[i][0]))
    else:
        return 10 + score_frame(allFrames[i + 1][0], allFrames, i + 1, 3)


def score_strike(allFrames, i, depth):
    if depth >= 2 :
        if isStrike(allFrames[i]):
            return 10
        else:
            return(int(allFrames[i][0]))
    else:
        if isStrike(allFrames[i + 1]):
            return 10 + score_frame(allFrames[i + 1], allFrames, i + 1, depth+1)
        else:
            return 10 + score_frame(allFrames[i + 1], allFrames, i + 1, depth+2)

def isStrike(frame):
    return frame == "X"

def isSpare(frame):
    return len(frame) > 1 and frame[1] == "/"
