import unittest
from codingdojo2.core import bowling

class Codingdojo2Testsuite(unittest.TestCase):

    def test_allZeroes_give_0(self):
        score = bowling("00 00 00 00 00 00 00 00 00 00")
        self.assertEqual(0, score)

    def test_allOnes_give_20(self):
        score = bowling("11 11 11 11 11 11 11 11 11 11")
        self.assertEqual(20, score)

    def test_01010101010101010101_gives_10(self):
        score = bowling("01 01 01 01 01 01 01 01 01 01")
        self.assertEqual(10, score)

    def test_01000000000000000000_gives_1(self):
        score = bowling("01 00 00 00 00 00 00 00 00 00")
        self.assertEqual(1, score)

    def test_02000000000000000000_gives_2(self):
        score = bowling("02 00 00 00 00 00 00 00 00 00")
        self.assertEqual(2, score)

    def test_03000000000000000000_gives_3(self):
        score = bowling("03 00 00 00 00 00 00 00 00 00")
        self.assertEqual(3, score)

    def test_X000000000000000000_gives_10(self):
        score = bowling("X 00 00 00 00 00 00 00 00 00")
        self.assertEqual(10, score)

    def test_X110000000000000000_gives_14(self):
        score = bowling("X 11 00 00 00 00 00 00 00 00")
        self.assertEqual(14, score)

    def test_1S110000000000000000_gives_13(self):
        score = bowling("1/ 11 00 00 00 00 00 00 00 00")
        self.assertEqual(13, score)

    def test_0000000000000000000S0_gives_10(self):
        score = bowling("00 00 00 00 00 00 00 00 00 0/ 0")
        self.assertEqual(10, score)

    def test_000000000000000000XXX_gives_30(self):
        score = bowling("00 00 00 00 00 00 00 00 00 X X X")
        self.assertEqual(30, score)

    def test_perfectGame_gives_300(self):
        score = bowling("X X X X X X X X X X X X")
        self.assertEqual(300, score)

    def test_000000000000000000X5S_gives_20(self):
        score = bowling("00 00 00 00 00 00 00 00 00 X 5/")
        self.assertEqual(20, score)

    def test_000000000000000000X53_gives_18(self):
        score = bowling("00 00 00 00 00 00 00 00 00 X 53")
        self.assertEqual(18, score)

    def test_0000000000000000001S1_gives_11(self):
        score = bowling("00 00 00 00 00 00 00 00 00 1/ 1")
        self.assertEqual(11, score)

    def test_0000000000000000001SX_gives_20(self):
        score = bowling("00 00 00 00 00 00 00 00 00 1/ X")
        self.assertEqual(20, score)

    def test_00005SX000000000000_gives_30(self):
        score = bowling("00 00 5/ X 00 00 00 00 00 00")
        self.assertEqual(30, score)

    def test_00005S50000000000000_gives_20(self):
        score = bowling("00 00 5/ 50 00 00 00 00 00 00")
        self.assertEqual(20, score)

    def test_00003S50000000000000_gives_20(self):
        score = bowling("00 00 3/ 50 00 00 00 00 00 00")
        self.assertEqual(20, score)

    def test_00003S4S000000000000_gives_24(self):
        score = bowling("00 00 3/ 4/ 00 00 00 00 00 00")
        self.assertEqual(24, score)
        


if __name__ == '__main__':
    unittest.main()